const path = require('path');

const prod = process.env.NODE_ENV === 'production';
const ghpages = process.env.VUE_APP_MODE === 'ghpages';

module.exports = {
  publicPath: ghpages
    ? '/generator-admin/'
    : '/',
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        path.resolve(__dirname, 'src/scss/core/*.scss'),
      ],
    },
  },
};