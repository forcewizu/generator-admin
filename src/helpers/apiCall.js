const theme = (name = 'healthy') => {
  return {
    name,
    image: require('@/assets/images/Clipboard01.jpg'),
    images: [
      require('@/assets/images/healthy/PC-article.png'),
      require('@/assets/images/healthy/PC-info.png'),
      require('@/assets/images/healthy/PC-main.png'),
      require('@/assets/images/healthy/PC-section.png'),
      require('@/assets/images/healthy/tab-info.png'),
      require('@/assets/images/healthy/tab-article.png'),
      require('@/assets/images/healthy/tab-main.png'),
      require('@/assets/images/healthy/tab-section.png'),
      require('@/assets/images/healthy/mob-nfo.png'),
      require('@/assets/images/healthy/mob-main.png'),
      require('@/assets/images/healthy/mob-article.png'),
      require('@/assets/images/healthy/mob-section.png'),
    ],
    id: Math.floor(Math.random() * (100000 - 1)) + 1,
  }
};

const user = ({email}) => {
  return {
    token: 'token',
    data: {
      name: 'Admin',
      email,
    }
  }
}

const themes = [];

const api = {
  getThemes: () => {
    return new Promise((resolve, reject) => {
      for (let i = 1; i < 12; i++) {
        themes.push(theme());
      }
      setTimeout(() => resolve(themes), 1500);
    })
  },
  getTheme: ({name}) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(theme(name)), 1500)
    })
  },
  login: (data) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(user(data)), 1500)
    })
  }
}

const apiCall = async (type, params) => {
  return await api[type](params);
}

export default apiCall;
