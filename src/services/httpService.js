import apiCall from '../helpers/apiCall';
import api from '../api';
import axios from 'axios';

const dev = process.env.NODE_ENV === 'development';

const http = apiCall;

class HttpService {

  generateSite = async (formData) => {
    try {
      const { data } = await axios.get(`${api.generate}?${formData}`);
      if (Number(data.code) !== 200 ) {
        console.error(api.generate, data);
        throw data;
      }
      return data;
    } catch(e) {
      throw { message: 'Something wrong' };
    }
  }
}

export { http };
export default HttpService;