import Vue from 'vue';
import Router from 'vue-router';
import store from './store';

Vue.use(Router)

const isAuth = () => store.getters['auth/isAuth'];

const ifAuthenticated = (to, from, next) => {
  if (isAuth()) {
    next();
  } else {
    next('/login');
  }
};

const ifNotAuthenticated = (to, from, next) => {
  if (!isAuth()) {
    next();
  } else {
    next('/');
  }
};

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue'),
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue'),
      beforeEnter: ifNotAuthenticated
    },
    {
      path: '/theme/:name',
      name: 'theme',
      component: () => import(/* webpackChunkName: "home" */ './views/Theme.vue'),
      beforeEnter: ifAuthenticated,
    },
  ]
})
