import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import SuiVue from 'semantic-ui-vue';
import 'semantic-ui-css/semantic.min.css';
import VueGlide from 'vue-glide-js'
import 'vue-glide-js/dist/vue-glide.css'
import '@/scss/style.scss';
import Message from 'vue-m-message';

Vue.use(Message);
Vue.use(VueGlide);
Vue.use(SuiVue);
Vue.config.productionTip = false

new Vue({ 
  router,
  store,
  render: h => h(App)
}).$mount('#app')
