const colors = [
  {
    key: 'red',
    text: 'Red',
    value: 'red',
    label: { color: 'red', empty: true, circular: true },
  },
  {
    key: 'blue',
    text: 'Blue',
    value: 'blue',
    label: { color: 'blue', empty: true, circular: true },
  },
  {
    key: 'black',
    text: 'Black',
    value: 'black',
    label: { color: 'black', empty: true, circular: true },
  },
  {
    key: 'purple',
    text: 'Purple',
    value: 'purple',
    label: { color: 'purple', empty: true, circular: true },
  },
  {
    key: 'orange',
    text: 'Orange',
    value: 'orange',
    label: { color: 'orange', empty: true, circular: true },
  },
  {
    key: 'yellow',
    text: 'Yellow',
    value: 'yellow',
    label: { color: 'yellow', empty: true, circular: true },
  },
  {
    key: 'pink',
    text: 'Pink',
    value: 'pink',
    label: { color: 'pink', empty: true, circular: true },
  },
  {
    key: 'green',
    text: 'Green',
    value: 'green',
    label: { color: 'green', empty: true, circular: true },
  },
];

export default colors;