const fonts = [
  { key: 'OpenSans', value: 'OpenSans', text: 'Open Sans' },
  { key: 'TimesNewRoman', value: 'TimesNewRoman', text: 'Times New Roman' },
  { key: 'Roboto', value: 'Roboto', text: 'Roboto' },
  { key: 'DMSerifDisplay', value: 'DMSerifDisplay', text: 'DM Serif Display' },
  { key: 'Literata', value: 'Literata', text: 'Literata' },
  { key: 'Lato', value: 'Lato', text: 'Lato' },
  { key: 'Montserrat', value: 'Montserrat', text: 'Montserrat' },
  { key: 'Ubuntu', value: 'Ubuntu', text: 'Ubuntu' },
  { key: 'Arial', value: 'Arial', text: 'Arial' },
];

export default fonts;