import HttpService from '@/services/httpService';
import fonts from './data/fonts';
import { GENERATE } from '../../actionTypes';

const { generateSite } = new HttpService();

function LightenDarkenColor(col, amt) {
  
  var usePound = false;

  if (col[0] == "#") {
      col = col.slice(1);
      usePound = true;
  }

  var num = parseInt(col,16);

  var r = (num >> 16) + amt;

  if (r > 255) r = 255;
  else if  (r < 0) r = 0;

  var b = ((num >> 8) & 0x00FF) + amt;

  if (b > 255) b = 255;
  else if  (b < 0) b = 0;

  var g = (num & 0x0000FF) + amt;

  if (g > 255) g = 255;
  else if (g < 0) g = 0;

  return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);

}
const state = {
  fonts: [...fonts],
  numbers: [5, 10, 15, 20, 30, 50],
  selectedColor: null,
  selectedFont: 'OpenSans',
  selectedFontSize: 16,
  selectedNumber: 5,
};

const mutations = {
  updateColors(state, data) {
    state.colors = [...data];
  },
  updateFonts(state, data) {
    state.fonts = [...data];
  },
  updateColor(state, data) {
    state.selectedColor = data.slice(1);
  },
  updateFontSize(state, data) {
    state.selectedFontSize = data;
  },
  updateFont(state, data) {
    state.selectedFont = data;
  },
  updateNumber(state, data) {
    state.selectedNumber = data;
  },
};

const getters = {
  getColor: (state) => {
    return `#${state.selectedColor}`;
  },
  getLightenColor: (state) => {
    if (state.selectedColor) {
      return LightenDarkenColor(state.selectedColor, 40);
    }
  },
  getDarkenColor: (state) => {
    if (state.selectedColor) {
      return LightenDarkenColor(state.selectedColor, -10);
    }
  },
  style: (state, getters) => {
    const fonts = {
      '--base-size': `${state.selectedFontSize}px`,
      '--base-font': state.selectedFont,
    }
    if (!state.selectedColor) {
      return fonts;
    }
    const style = Object.assign({}, fonts, {
      '--lighten-bg-color': getters.getLightenColor,
      '--darken-bg-color': getters.getDarkenColor,
      '--bg-color': getters.getColor,
    })
    return style;
  },
  disabledGenerate: (state) => {
    return state.selectedColor && state.selectedFont && state.selectedFontSize;
  },
};

const actions = {
  async [GENERATE]({ state }, TEMPLATE) {
    const formData = `TEMPLATE=${TEMPLATE}` +
      `&MY_COLOR=${state.selectedColor}` +
      `&MY_FONT=${state.selectedFont}` +
      `&MY_FONT_SIZE=${state.selectedFontSize}` +
      `&MY_NUMBER=${state.selectedNumber}`;
    console.table(formData);
    try {
      const data = await generateSite(formData);
      return data;
    } catch (e) {
      throw e;
    }
    
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};