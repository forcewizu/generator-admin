import { http } from '@/services/httpService';
import router from '@/router';

const state = {
  token: localStorage.getItem('admin-generator-token') || '',
  user: {
    name: '',
    email: '',
    id: '',
  },
  authStatus: '',
};

const mutations = {
  AUTH_REQUEST: state => {
    state.authStatus = 'loading';
  },
  AUTH_SUCCESS: (state, {data, token}) => {
    localStorage.setItem('admin-generator-token', token);
    state.authStatus = 'success';
    state.user = {...data};
    state.token = token;
  },
  AUTH_ERROR: state => {
    localStorage.removeItem('admin-generator-token');
    state.authStatus = 'error';
  },
  AUTH_LOGOUT: state => {
    localStorage.removeItem('admin-generator-token');
    state.token = '';
  },
};

const getters = {
  isAuth: state => !!state.token,
};

const actions = {
  async AUTH_REQUEST({ commit }, user)  {
    commit('AUTH_REQUEST');
    try {
      const data = await http('login', user);
      commit('AUTH_SUCCESS', data);
      return;
    } catch(e) {
      commit('AUTH_ERROR');
      console.log(error, e);
    }
  },
  AUTH_LOGOUT({ commit }) {
    commit('AUTH_LOGOUT');
    router.push({name: 'login'});
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};