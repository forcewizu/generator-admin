import { http } from '@/services/httpService';

const state = {
  themes: [],
  theme: {
    name: '',
    image: '',
    images: [],
    id: null,
  },
  loading: false,
  error: false,
};

const mutations = {
  updateThemes(state, data) {
    state.themes = [...data];
  },
  updateTheme(state, data) {
    state.theme = {...data};
  },
  loading(state, param) {
    state.loading = param;
  }
};

const getters = {

};

const actions = {
  async getThemes({commit}) {
    commit('loading', true);
    try {
      const data = await http('getThemes');
      commit('updateThemes', data);
    } catch(e) {
      console.log(error, e);
    }
    commit('loading', false);
  },
  async getTheme({commit}, name) {
    commit('loading', true);
    try {
      const data = await http('getTheme', {name});
      commit('updateTheme', data);
    } catch(e) {
      console.log(error, e);
    }
    commit('loading', false);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};