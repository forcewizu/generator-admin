import Vue from 'vue'
import Vuex from 'vuex'
import themes from './modules/themes';
import settings from './modules/settings';
import auth from './modules/auth';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    themes,
    settings,
    auth,
  }
})
